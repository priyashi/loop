////////////////////////////  for benefit section svg stroke start on scroll /////////////////////////////////////
$( document ).ready(function() {
$(function() {
    $(window).scroll(function() {
        var scroll = $(window).scrollTop();

        if (scroll >= 80) {
            $("#path-bg__progress").css("animation","40s ease infinite wander-path 900ms");
              $("#path-bg__progress").css("-webkit-animation","40s ease infinite wander-path 800ms");
        } else {
            $("#path-bg__progress").css("animation","none");
              $("#path-bg__progress").css("-webkit-animation","none");
        }
    });
});
});
//////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function(){

  $("#ourstory").click(function(){
    var symbol = $("#video1")[0].src.indexOf("?") > -1 ? "&" : "?";
  //modify source to autoplay and start video
$("#video1").attr('src', 'https://www.youtube.com/embed/-iOj0vaxtro');

  $("#video1")[0].src += symbol + "autoplay=1";
});

$('#youtube_video').on('hidden.bs.modal', function () {
   $("#video1").attr('src', '');
 // $('#video1')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
   });

});



//////////////////// for mouseover on team member ////////////////////////////////////////////
$( document ).ready(function() {
$( ".founder1" ).hover(
  function() {
    $( this ).append( $( "<section class='team_members' style='animation: fadein 2s;width:500px;height:150px;position:absolute;background: white;z-index: 2; border: 2px solid #EE603D;  border-radius: 80px;margin-top: -119px;  margin-left: -5px;  -webkit-animation: animateThis 0.2s ease-in;-webkit-animation-fill-mode: forwards;  color: #EE603D;'>"
    +"<img style='width:148px;height:148px;margin-top: -1px; border: 2px solid #EE603D; margin-left:-2px;' class='detail_img' src='img/team/founder1.png' alt=''>"
    +"<div style='display:flex;justify-content:space-between; animation: fadein 1s; margin-top:-137px;margin-left:162px; font-size:18px; width:230px; font-weight:700;'><p style='margin-top:-3px'>Aditya Sethi</p><a id='hoveron' href='mailto:aditya@getloopapp.com'><div class='show_tooltip1'><i class='fa fa-caret-down arrow-down' aria-hidden='true'></i>aditya@getloopapp.com</div><img src='img/team/popup/mail.png' style='width:34px; height:34px; margin-left:1.5rem;' alt='mail'/></a><a href='https://www.linkedin.com/in/adityasethi/' target='_blank'><img src='img/team/popup/linkedin.png' style='width:34px; height:34px;' alt='linkedin'/></a></div><p style='font-size:16px; margin-top:-22px;margin-left:162px; animation: fadein 1s;'>Founder</p>"
    +"<div style='display:flex;animation: fadein 0.8s;'><img src='img/team/popup/education.png' style='width:20px; height:20px ;margin-left:160px;margin-top: -10px;' alt='university'/><p style='font-size:14px; margin-top:-12px;color:#454545; margin-left:12px;'>University of Waterloo</p></div>"
    +"<div style='display:flex;animation: fadein 0.8s;'><img src='img/team/popup/qualification.png' style='width:20px; height:20px; margin-left:160px;border-radius:0;margin-top: -10px;' alt='work'/><p style='font-size:14px; margin-top:-12px;color:#454545;margin-left:12px;'>Barclays Capital, TD Asset Management, Digital Green </p></div>"
  +"</section>") );
     $(".img_effect").addClass("img_blur");
  }, function() {
    $( this ).find( "section:last" ).remove();
       $(".img_effect").removeClass("img_blur");
  },
   function () {
  $('a').tooltip()
}
);

$( ".founder2" ).hover(
  function() {
    $( this ).append( $( "<section class='team_members' style='animation: fadein 2s;width:500px;height:150px;position:absolute;background: white;z-index: 2; border: 2px solid #EE603D;  border-radius: 80px;margin-top: -119px;  margin-left: -5px;  -webkit-animation: animateThis 0.2s ease-in;-webkit-animation-fill-mode: forwards;  color: #EE603D;'>"
    +"<img style='width:148px;height:148px;margin-top: -1px; border: 2px solid #EE603D; margin-left:-2px;' class='detail_img' src='img/team/founder2.png' alt=''>"
    +"<div style='display:flex;justify-content:space-between; animation: fadein 1s; margin-top:-137px;margin-left:162px; font-size:18px; width:230px; font-weight:700;'><p style='margin-top:-3px'>Saureen Shah</p><a id='hoveron' href='mailto:saureen@getloopapp.com'><div class='show_tooltip1'><i class='fa fa-caret-down arrow-down' aria-hidden='true'></i>saureen@getloopapp.com</div><img src='img/team/popup/mail.png' style='width:34px; height:34px; margin-left:1.5rem;' alt='mail'/></a><a href='https://www.linkedin.com/in/saureenshah/' target='_blank'><img src='img/team/popup/linkedin.png' style='width:34px; height:34px;' alt='linkedin'/></a></div><p style='font-size:16px; margin-top:-22px;margin-left:162px; animation: fadein 1s;'>Founder</p>"
    +"<div style='display:flex;animation: fadein 0.8s;'><img src='img/team/popup/education.png' style='width:20px; height:20px ;margin-left:160px;margin-top: -10px;' alt='university'/><p style='font-size:14px; margin-top:-12px;color:#454545; margin-left:12px;'>Carnegie Mellon University</p></div>"
    +"<div style='display:flex;animation: fadein 0.8s;'><img src='img/team/popup/qualification.png' style='width:20px; height:20px; margin-left:160px;border-radius:0;margin-top: -10px;' alt='work'/><p style='font-size:14px; margin-top:-12px;color:#454545;margin-left:12px;'>Early YouTuber, Co-founder/ CTO Instawork</p></div>"
  +"</section>") );
     $(".img_effect").addClass("img_blur");
  }, function() {
    $( this ).find( "section:last" ).remove();
       $(".img_effect").removeClass("img_blur");
  }
);

$( ".founder3" ).hover(
  function() {
    $( this ).append( $( "<section class='team_members' style='animation: fadein 2s;width:500px;height:150px;position:absolute;background: white;z-index: 2; border: 2px solid #EE603D;  border-radius: 80px;margin-top: -119px;  margin-left: -5px;  -webkit-animation: animateThis 0.2s ease-in;-webkit-animation-fill-mode: forwards;  color: #EE603D;'>"
    +"<img style='width:148px;height:148px;margin-top: -1px; border: 2px solid #EE603D; margin-left:-2px;' class='detail_img' src='img/team/founder3.png' alt=''>"
    +"<div style='display:flex;justify-content:space-between; animation: fadein 1s; margin-top:-137px;margin-left:162px; font-size:18px; width:230px; font-weight:700;'><p style='margin-top:-3px'>Rikin Gandhi</p><a id='hoveron' href='mailto:rikin@getloopapp.com'><div class='show_tooltip1'><i class='fa fa-caret-down arrow-down' aria-hidden='true'></i>rikin@getloopapp.com</div><img src='img/team/popup/mail.png' style='width:34px; height:34px; margin-left:1.5rem;' alt='mail'/></a><a href='https://www.linkedin.com/in/rikingandhi/' target='_blank'><img src='img/team/popup/linkedin.png' style='width:34px; height:34px;' alt='linkedin'/></a></div><p style='font-size:16px; margin-top:-22px;margin-left:162px; animation: fadein 1s;'>Founder</p>"
    +"<div style='display:flex;animation: fadein 0.8s;'><img src='img/team/popup/education.png' style='width:20px; height:20px ;margin-left:160px;margin-top: -10px;' alt='university'/><p style='font-size:14px; margin-top:-12px;color:#454545; margin-left:12px;'>Carnegie Mellon University</p></div>"
    +"<div style='display:flex;animation: fadein 0.8s;'><img src='img/team/popup/qualification.png' style='width:20px; height:20px; margin-left:160px;border-radius:0;margin-top: -10px;' alt='work'/><p style='font-size:14px; margin-top:-12px;color:#454545;margin-left:12px;'>MIT, Oracle, Microsoft Research, Digital Green</p></div>"
  +"</section>") );
     $(".img_effect").addClass("img_blur");
  }, function() {
    $( this ).find( "section:last" ).remove();
       $(".img_effect").removeClass("img_blur");
  }
);
});
/////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////// code for index page ///////////////////////////////////
$(document).ready(function() {
  $('body').scrollspy({
    target: '#scroll-spy',
    offset: 154
  });

  var height = $('#meet').innerHeight();
  var windowHeight = $(window).height();
  var navHeight = $('nav.navbar').innerHeight();
  var siblingHeight = $('#meet').nextAll().innerHeight();
  var shiftWindow = function() {
    scrollBy(16, -96)
  };
  if (location.hash) shiftWindow();
  window.addEventListener("hashchange", shiftWindow);

  if (height < windowHeight) {
    $('body').css("padding-bottom", windowHeight - navHeight - height - siblingHeight + "px");
  }

  function myFunction(x) {
    if (x.matches) { // If media query matches
      $('.sub_nav').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(sectionTo).offset().top - 300
        }, 1000);
      });
    }
  else  if (y.matches) { // If media query matches
      $('.sub_nav').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(sectionTo).offset().top - 290
        }, 1000);
      });
    }
     else {

      $('.sub_nav').click(function() {
        var sectionTo = $(this).attr('href');
        $('html, body').animate({
          scrollTop: $(sectionTo).offset().top - 40
        }, 1000);
      });
    }
  }

  var x = window.matchMedia("(max-width: 767px)");
  var y = window.matchMedia("(min-width: 768px) and (max-width: 1023px)");
  myFunction(x) // Call listener function at run time
  x.addListener(myFunction) // Attach listener function on state changes
/////////////////////////////////////////////////////////////////////////////////////////

///////////////////////  for animate on scroll on clicking logo ///////////
        $('#brand_logo').click(function() {
          // var sectionTo = $(this).attr('href');
          $('html, body').animate({
            scrollTop: $('#top_of_page').offset().top
          }, 1000);
        });
//////////////////////////////////////////////////////////////////////////


////////////////////////// Remove navbar on select //////////////////////
  $('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggler:visible').click();
        $('#nav-icon3').toggleClass('open');
  });
});
/////////////////////////////////////////////////////////////////////////

///////////////////// gif on scroll ////////////////////////////////
$(document).ready(function() {
    function myFunction(x) {
      if (x.matches) { // If media query matches
        $(window).scroll(function() {
       var algo_sec = $('.algorithm').outerHeight() + 1400;
       // console.log(algo_sec);
          var current = $(this).scrollTop(),
          path = 'img/loopanimation.gif',
          visible = $('.gifplay').css('opacity') != 1;
      // console.log(current);
          if (current > algo_sec) {
          if (!visible) $('.gifplay').attr('src', path).fadeTo(400,1);
          }
          else if (visible) $('.gifplay').fadeTo(0,0);
        });
      }
    else  if (y.matches) { // If media query matches
      $(window).scroll(function() {
     var algo_sec = $('.algorithm').outerHeight() + 1650;
     // console.log(algo_sec);
        var current = $(this).scrollTop(),
        path = 'img/loopanimation.gif',
        visible = $('.gifplay').css('opacity') != 1;
    // console.log(current);
        if (current > algo_sec) {
        if (!visible) $('.gifplay').attr('src', path).fadeTo(400,1);
        }
        else if (visible) $('.gifplay').fadeTo(0,0);
      });
      }
       else {

         $(window).scroll(function() {
        var algo_sec = $('.algorithm').outerHeight() + 2750;
        // console.log(algo_sec);
           var current = $(this).scrollTop(),
           path = 'img/loopanimation.gif',
           visible = $('.gifplay').css('opacity') != 1;
       // console.log(current);
           if (current > algo_sec) {
           if (!visible) $('.gifplay').attr('src', path).fadeTo(400,1);
           }
           else if (visible) $('.gifplay').fadeTo(0,0);
         });
      }
    }

  var x = window.matchMedia("(max-width: 767px)");
  var y = window.matchMedia("(min-width: 768px) and (max-width: 1023px)");
  myFunction(x) // Call listener function at run time
  x.addListener(myFunction) // Attach listener function on state changes

	});
/////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////  for carousel /////////////////////////////////////
$(document).ready(function() {
$("#bannercard").owlCarousel({
  loop: true,
  center: false,
  margin: 30,
  autoplay: true,
  dots: true,
  nav: false,
  navText: [$('.prev-btn'), $('.next-btn')],
  responsive: {
    0: {
      items: 1
    },
    768: {
      items: 1
    },
    1170: {
      items: 1
    }
  }
});
});
/////////////////////////////////////////////////////////////////////////////////////////

///////////////////////  for get in touch number validation ///////////

function phonenumber(){
var num = document.getElementById("phone1").value;
if (isNaN(num)){
$("#num").css("visibility","visible");
$("#getcall_submit_btn").prop('disabled', true);
  return false;
}else{
    $("#num").css("visibility","hidden");
      $("#getcall_submit_btn").prop('disabled', false);
  return true;

  }
}
/////////////////////////////////////////////////////////////////////////////////////////

///////////////////////  for get a call number validation ///////////

function contactnumber(){
var num = document.getElementById("phone").value;
if (isNaN(num)){
$("#num1").css("visibility","visible");
$("#callback_submit_btn").prop('disabled', true);
  return false;
}else{
    $("#num1").css("visibility","hidden");
    $("#callback_submit_btn").prop('disabled', false);
  return true;

  }
}
/////////////////////////////////////////////////////////////////////////////////////////

///////////////////////  for get a call number validation ///////////

function emailaddress(){
var num = document.getElementById("email").value;

var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  // if(!emailReg.test(emailaddress)) {
  //      alert("Please enter valid email id");
  // }

if (!emailReg.test(num)){
$("#emailnum").css("visibility","visible");
$("#callback_submit_btn").prop('disabled', true);
  return false;
}else{
    $("#emailnum").css("visibility","hidden");
    $("#callback_submit_btn").prop('disabled', false);
  return true;

  }
}
/////////////////////////////////////////////////////////////////////////////////////////
